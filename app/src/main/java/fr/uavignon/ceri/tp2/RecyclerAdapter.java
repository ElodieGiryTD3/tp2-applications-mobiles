package fr.uavignon.ceri.tp2;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.tp2.RecyclerAdapter.ViewHolder> {

    private int bookItemLayout;
    private List<Book> bookList;
    //private BookDao bookDao;

    private BookRepository repository;

    private ListViewModel viewModel;

    private static final String TAG = fr.uavignon.ceri.tp2.RecyclerAdapter.class.getSimpleName();

    //EG ajout du constructeur pour créer un objet adapter (récupéré de Romm Demo)
    public RecyclerAdapter(int bookId) {
        bookItemLayout = bookId;
    }

    public void setBookList(List<Book> books){
        bookList = books;
        //notifie la modification d'une donnée pour rafraichir l'affichage
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    //EG UTILISER BDD PAS BOOKS
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Book book;
        viewHolder.itemTitle.setText(bookList.get(i).getTitle());
        viewHolder.itemDetail.setText(bookList.get(i).getAuthors());
    }

    //EG UTILISER BDD PAS BOOKS
    @Override
    public int getItemCount() {
        //return repository.nbBooks();
        return bookList == null ? 0 : bookList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);

            int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    //EG On remplace la position du livre par son id pour la sélection
                    //int position = getAdapterPosition();

                    Log.d("","avant getId");
                    long id = RecyclerAdapter.this.bookList.get((int)getAdapterPosition()).getId();

                    /* Snackbar.make(v, "Click detected on chapter " + (position+1),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                     */
                    Log.d("","avant action");

                    //pour ajouter un book
                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                    //EG on remplace position par id
                    //action.setBookNum(position);
                    action.setBookNum(id);
                    Navigation.findNavController(v).navigate(action);


                }
            });
        }
    }
}
