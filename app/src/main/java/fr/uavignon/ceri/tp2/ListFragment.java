package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class ListFragment extends Fragment {

    private TextView bookId;
    private TextView bookTitle;
    private TextView bookAuthors;

    //EG ajout de la variable viewModel pour le modèle de vue
    private ListViewModel viewModel;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerAdapter adapter;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);

        //EG ajout pour associer le modèle de vue au fragment
        observerSetup(); // enregistrement du fragment comme observateur du ViewModel
        recyclerSetup(); // association de l'adaptateur à la RecyclerView

        //EG récupéré de Room Demo
        recyclerView = view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        //EG Ajout du paramètre dans le constructeur
        adapter = new RecyclerAdapter(R.layout.fragment_list);
        recyclerView.setAdapter(adapter);


        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                action.setBookNum(-1);
                Navigation.findNavController(view).navigate(action);
            }
        });
    }

    //EG pour mise à jour des livres à afficher
    private void observerSetup() {
        viewModel.getAllBooks().observe(getViewLifecycleOwner(),
                new Observer<List<Book>>() {
                    @Override
                    public void onChanged(@Nullable final List<Book> books) {
                        adapter.setBookList(books);
                    }
                });
    }

    //EG récupéré du modèle Room Demo
    private void recyclerSetup() {
        RecyclerView recyclerView;
        adapter = new RecyclerAdapter(R.layout.fragment_list);
        recyclerView = getView().findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }
}
