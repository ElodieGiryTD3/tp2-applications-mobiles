package fr.uavignon.ceri.tp2;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailViewModel extends AndroidViewModel {

    //EG Lien entre dépôt et BDD
    private BookRepository repository;
    //EG Liste où on enregistre les livres correspondant à la sélection de l'utilisateur
    private MutableLiveData<Book> book;

    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
    }

    public void insertBook(Book newBook) {
        Log.d("coucou","coucou");
        if (book.getValue() == null) {
            Log.d("dansInsert","coucou");
            repository.insertBook(newBook);
        }
    }
    public void updateBook(Book book) {
        repository.updateBook(book);
    }
    public void deleteBook(long id) {
        repository.deleteBook(id);
    }
    public void deleteAllBooks() {
        repository.deleteAllBooks();
    }
    public Book getBook(long id) {
        if(id==-1){
            book = new MutableLiveData(null);
        }
        else{
            book = new MutableLiveData(repository.getBook(id));
        }
        return book.getValue();
    }
}

