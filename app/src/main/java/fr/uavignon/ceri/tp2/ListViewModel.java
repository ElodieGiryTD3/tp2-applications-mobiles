package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import java.util.List;
import fr.uavignon.ceri.tp2.data.Book;

public class ListViewModel extends AndroidViewModel {

    //EG Lien entre dépôt et BDD
    private BookRepository repository;
    //EG Liste où on enregistre les livres de la BDD pour les afficher
    private LiveData<List<Book>> allBooks;

//EG ajout du constructeur pour utiliser le modèle de vue pour récupérer la liste de livres
    public ListViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        allBooks = repository.getAllBooks();
    }

    //EG ajout des fonctions d'accès à la BDD
    public void insertBook(Book book) {
        repository.insertBook(book);
    }

    public void updateBook(Book book) {
        repository.updateBook(book);
    }

    public void deleteBook(long id) {
        repository.deleteBook(id);
    }
    public void deleteAllBooks() {
        repository.deleteAllBooks();
    }

    public void getBook(long id) {
        repository.getBook(id);
    }

    LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public int nbBooks(){
        return repository.nbBooks();
    }
}
