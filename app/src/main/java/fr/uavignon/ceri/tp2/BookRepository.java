package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp2.data.Book;

import static fr.uavignon.ceri.tp2.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {

    //EG attribut pour afficher le livre à modifier
    private MutableLiveData <Book> selectedBook = new MutableLiveData<>();

    //EG attribut pour afficher les livres
    private LiveData<List<Book>> allBook;

    private BookDao bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBook = bookDao.getAllBooks();
    }

    public MutableLiveData<Book> getSearchResults() {
        return selectedBook;
    }

    //EG Définition des fonctions pour mettre à jour la BDD

    public void updateBook(Book book){
        BookRoomDatabase.databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(book);
        });
    }

    public void insertBook(Book book){
        selectedBook.setValue(book);
        BookRoomDatabase.databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(book);
        });

    }

    public Book getBook(long id){
        Book book;
        Future<Book> fbooks = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            book = (Book) fbooks.get();
            return book;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteBook(long id) {
        BookRoomDatabase.databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }

    public LiveData <List<Book>> getAllBooks() {
        return allBook;
    }

    public void deleteAllBooks() {
        BookRoomDatabase.databaseWriteExecutor.execute(() -> {
            bookDao.deleteAllBooks();
        });
    }

    public int nbBooks() {
        BookRoomDatabase.databaseWriteExecutor.execute(() -> {
            bookDao.nbBooks();
        });
        return bookDao.nbBooks();
    }
}
