package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.BookRoomDatabase;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;

    private DetailViewModel viewModel;

    //EG 15.03 test update
    //BookDao dao = INSTANCE.bookDao();
    private BookRepository bookInsert;

    //public abstract BookDao bookDao();
    //private static volatile BookRoomDatabase INSTANCE;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());

        //NE PLUS UTILISER LE TABLEAU STATIQUE BOOKS
        //Book book = Book.books[(int)args.getBookNum()];
        Book book = viewModel.getBook(args.getBookNum());

        
        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        if(book != null){
            textTitle.setText(book.getTitle());
            textAuthors.setText(book.getAuthors());
            textYear.setText(book.getYear());
            textGenres.setText(book.getGenres());
            textPublisher.setText(book.getPublisher());

        }


        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        view.findViewById(R.id.buttonDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.deleteBook(book.getId());
            }
        });

        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //long id = book.getId();

                String titre = textTitle.getText().toString();
                String auteur = textAuthors.getText().toString();
                String annee = textYear.getText().toString();
                String genre = textGenres.getText().toString();
                String editeur = textPublisher.getText().toString();

                //Si le livre existe on le met à jour
                if(book!=null){
                    book.setTitle(titre);
                    book.setAuthors(auteur);
                    book.setYear(annee);
                    book.setGenres(genre);
                    book.setAuthors(editeur);
                    viewModel.updateBook(book);
                }
                //Si le livre n'existe pas on le crée
                else{
                    Book book = new Book(titre,auteur,annee,genre,editeur);
                    viewModel.insertBook(book);
                }

                //Book book = new Book("Tutu", "pgiry", "1957", "roman de science-fiction", "Hachette");
                //book.setId(1);
            }
        });
    }
}
